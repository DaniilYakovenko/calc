import Memory from './memory';

export const OPERATIONS = {
    CALC: 'calc',
    SUM: 'sum',
    DEDUCT: 'deduct',
    PERCENT: 'percent',
    MULTIPLY: 'multiply',
    DIVIDE: 'divide',
    MEMORYPLUS: 'memory-plus',
    MEMORYREAD: 'memory-read',
    MEMORYMINUS: 'memory-minus',
    MEMORYCLEAR: 'memory-clear',
    POWER: 'power',
    PI: 'pi',
    EXP: 'exp',
    RADICAL: 'radical',
    OPENBRACKET: 'openBracket',
    CLOSEBRACKET: 'closeBracket'
};
export const OPERATION_TYPES = {
    BINARY: 2,
    UNARY: 1,
    CALCULATION: 3
};
const PI = 3.14159265359;
const EXP = 2.71828182846;
function initCalculatorOperationsMap(container) {
    container.set(OPERATIONS.SUM, { symbol: '+', type: OPERATION_TYPES.BINARY, action: (a, b) => a + b });
    container.set(OPERATIONS.DEDUCT, { symbol: '-', type: OPERATION_TYPES.BINARY, action: (a, b) => a - b });
    container.set(OPERATIONS.DIVIDE, { symbol: '/', type: OPERATION_TYPES.BINARY, action: (a, b) => a / b });
    container.set(OPERATIONS.MULTIPLY, { symbol: '*', type: OPERATION_TYPES.BINARY, action: (a, b) => a * b });
    container.set(OPERATIONS.CALC, { symbol: '=', type: OPERATION_TYPES.CALCULATION });
    container.set(OPERATIONS.POWER, { symbol: '^2', type: OPERATION_TYPES.UNARY, action: a => a * a });
    container.set(OPERATIONS.RADICAL, { symbol: '√', type: OPERATION_TYPES.UNARY, action: a => a ** (1 / 2) });
    container.set(OPERATIONS.PI, { symbol: 'π', type: OPERATION_TYPES.UNARY, action: () => PI });
    container.set(OPERATIONS.EXP, { symbol: 'e', type: OPERATION_TYPES.UNARY, action: () => EXP });
    container.set(OPERATIONS.PERCENT, { symbol: '%', type: OPERATION_TYPES.UNARY, action: a => a / 100 });
};
export class CalculatorOperations {
    constructor() {
        this.operationsMap = new Map();
        this.memory = new Memory();
        this._initMemoryOperations();
        initCalculatorOperationsMap(this.operationsMap);
    }
    getOperation(operation) {
        return this.operationsMap.get(operation);
    }
    hasOperation(operation) {
        return this.operationsMap.has(operation);
    }
    _initMemoryOperations() {
        this.operationsMap.set(OPERATIONS.MEMORYCLEAR, { type: OPERATION_TYPES.UNARY, action: operand => { this.memory.clear(operand); return operand; } });
        this.operationsMap.set(OPERATIONS.MEMORYMINUS, { type: OPERATION_TYPES.UNARY, action: operand => { this.memory.deduct(operand); return operand; } });
        this.operationsMap.set(OPERATIONS.MEMORYPLUS, { type: OPERATION_TYPES.UNARY, action: operand => { this.memory.sum(operand); return operand; } });
        this.operationsMap.set(OPERATIONS.MEMORYREAD, { type: OPERATION_TYPES.UNARY, action: () => this.memory.getValue() });
    }
}