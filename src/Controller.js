import { OPERATIONS } from "./calculatorOperations";
import Calculator from "./Calculator";
import NumberBuffer from "./NumberBuffer";
import Stack from "./Stack";


class Controller {
    constructor(ea) {
        this.currentCalculator = new Calculator();
        this.numberBuffer = new NumberBuffer();
        this.ea = ea;
        this.workingWithSymbols = true;
        this.bracketsStack = new Stack();
    }
    _getOperationsHistory() {
        let history;
        if (this.bracketsStack.getCount() !== 0) {
            history = this.bracketsStack.asArray()[0].getOperationsHistory();
        }
        else {
            history = this.currentCalculator.getOperationsHistory();
        }
        if (Array.isArray(history) && history.length !== 0) {
            history = history.join(' ');
        }
        return history;
    }
    _emitValueChanged(value) {
        const val = value || this.numberBuffer.getStringValue();
        this.ea.emit('value-changed', val);
    }
    _emitOperationChanged(operation) {
        this.ea.emit('operation-changed', operation);
    }
    _changeWorkingPlaceToSymbols() {
        if (!this.workingWithSymbols) {
            this.workingWithSymbols = true;
        }
    }
    _changeWorkingPlaceFromSymbols() {
        if (this.workingWithSymbols) {
            this.workingWithSymbols = false;
            this.currentCalculator.execute(this.numberBuffer.getNumber());
        }
    }
    addSymbol(symbol) {
        this._changeWorkingPlaceToSymbols();
        this.numberBuffer.add(symbol);
        this._emitValueChanged();
    }
    setSymbols(symbols) {
        if (this.numberBuffer.set(symbols)) {
            this._changeWorkingPlaceToSymbols();
            this._emitValueChanged();
        }
    }
    removeSymbol() {
        this._changeWorkingPlaceToSymbols();
        this.numberBuffer.remove();
        this._emitValueChanged();
    }
    invert() {
        if (!this.workingWithSymbols) {
            this.numberBuffer.set(this.currentCalculator.getResult());
        }
        this._changeWorkingPlaceToSymbols();
        this.numberBuffer.invert();
        this._emitValueChanged();
    }
    executeOperation(operation) {
        if (operation === OPERATIONS.OPENBRACKET) {
            this._emitOperationChanged(operation);
            this.openBracket();
            return;
        }
        if (operation === OPERATIONS.CLOSEBRACKET) {
            this._emitOperationChanged(operation);
            this.closeBracket();
            return;
        }
        if (operation === OPERATIONS.MEMORYREAD) {
            this.currentCalculator.execute(1);
        }
        this._changeWorkingPlaceFromSymbols();
        this.currentCalculator.execute(operation);
        this._emitValueChanged(this.currentCalculator.getResult());
        this.ea.emit('history-changed', this._getOperationsHistory());
        this.numberBuffer.clear();
        this._emitOperationChanged(operation);
    }
    openBracket() {
        this.bracketsStack.add(this.currentCalculator);
        this.currentCalculator = new Calculator();
        this._changeWorkingPlaceToSymbols();
    }
    closeBracket() {
        let lastCalc = this.bracketsStack.get();
        if (lastCalc) {
            this._changeWorkingPlaceFromSymbols();
            this.currentCalculator.execute(OPERATIONS.CALC);
            this._emitValueChanged(this.currentCalculator.getResult());
            this.numberBuffer.clear();
            let result = this.currentCalculator.getResult();
            this.currentCalculator = lastCalc;
            this.addSymbol(result);
        }
    }
    clearAll() {
        this.ea.emit('history-changed', 'AC');
        this._changeWorkingPlaceToSymbols();
        this.numberBuffer.clear();
        this.currentCalculator.reset();
        this.bracketsStack.clear();
        this._emitValueChanged();
        this._emitOperationChanged(null);
    }
}
export default Controller;