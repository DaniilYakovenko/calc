const isNumber = /^-?\d*(\.?)\d*$/;

export class NumberBuffer {
    constructor() {
        this._value = '';
    }
    clear() {
        this._value = '';
    }
    set(value) {
        if (isNumber.test(value)) {
            this._value = value.toString();
            return true;
        }
    }
    add(symbol) {
        if (isNumber.test(this._value + symbol)) {
            this._value += symbol;
        }
    }
    getNumber() {
        let result = parseFloat(this._value || '0');
        return isNaN(result) ? '0' : result;
    }
    getStringValue() {
        let result = this.toString();
        return result ? result : '0';
    }
    toString() {
        if (isNumber.test(this._value) && /^\./.test(this._value)) {
            return '0' + this._value;
        }

        if (isNumber.test(this._value) && /\.$/.test(this._value)) {
            return this.getNumber() + '.';
        }

        return this._value;
    }
    invert() {
        if (/^-/.test(this._value)) {
            this._value = this._value.substring(1, this._value.length);
        }
        else {
            this._value = '-' + this._value;
        }
    }
    remove() {
        this._value = this._value.substring(0, this._value.length - 1);
    }
};

export default NumberBuffer;