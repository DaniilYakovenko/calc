class Memory {
    constructor() {
        this.value = 0;
    }
    sum(value) {
        this.value += value;
    }
    deduct(value) {
        this.value -= value;
    }
    getValue() {
        return this.value;
    }
    clear() {
        this.value = 0;
    }
}

export default Memory;