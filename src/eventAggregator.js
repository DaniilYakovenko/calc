; "use strict"
function EventAggregator() {
    this.subscribers = {};
    this.counter = 0;
}
EventAggregator.prototype.on = function (eventName, fn) {
    var self = this;
    if (!this.subscribers[eventName]) {
        this.subscribers[eventName] = [];
    }
    var index = this.subscribers[eventName].push(fn);
    return {
        unsubscribe: function () {
            self.subscribers[eventName].splice(index, 1);
        }
    }
};
EventAggregator.prototype.emit = function (eventName, arg) {
    if (this.subscribers[eventName]) {
        this.subscribers[eventName].forEach(function (item) {
            setTimeout(item.bind(this, arg), 0);
        });
    }
    else {
        console.log('у свойства ' + eventName + ' нету подписчиков');
    }
};
EventAggregator.prototype.unsubscribe = function (eventName) {
    this.subscribers[eventName].splice(-1, 1);
    if (!this.subscribers[eventName].length) {
        delete this.subscribers[eventName];
    }
};
EventAggregator.prototype.unsubscribeAll = function (eventName) {
    delete this.subscribers[eventName];
}

export default EventAggregator;