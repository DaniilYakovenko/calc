import Stack from './Stack';
import { OPERATION_TYPES, CalculatorOperations } from './calculatorOperations';

const DEFAULT_STRATEGY = () => { };

export class Calculator {
    constructor() {
        this.calculatorOperations = new CalculatorOperations();
        this.operationsHistory = new Stack(1);
        this.operands = new Stack(2);
        this.reset();
    }
    _addOperationToHistory(operation, operands, result) {
        if (Array.isArray(operands)) {
            this.operationsHistory.add([operands[0], operation, operands[1], '=', result]);
        }
        else {
            this.operationsHistory.add([operands, operation, '=', result]);
        }
    }
    getOperationsHistory() {
        return this.operationsHistory.get();
    }
    reset() {
        this.currentOperation = undefined;
        this.operands.clear();
        this.previousOperation = {};
        this.bufferCommand = undefined;
        this.bufferOperand = undefined;
        this.bufferOperation = undefined;
        this.result = undefined;
    }
    _setResult(result) {
        this.result = result;
    }
    _setBufferCommand(command) {
        this.bufferCommand = command;
    }
    _getBufferCommand() {
        return this.bufferCommand;
    }
    _setBufferOperand(operand) {
        this.bufferOperand = operand;
    }
    _getBufferOperand() {
        return this.bufferOperand;
    }
    _setBufferOperation(operation) {
        this.bufferOperation = operation;
    }
    _getBufferOperation() {
        return this.bufferOperation;
    }
    _setPreviousOperation(operation) {
        this.previousOperation = operation;
    }
    _setCurrentOperation(command) {
        this.currentOperation = this.calculatorOperations.getOperation(command);
    }
    _isOperand(command) {
        return typeof command === 'number';
    }
    _clearResult() {
        this.result = undefined;
    }
    _clearPreviousOperation() {
        this.previousOperation = {};
    }
    _calculate() {
        this._setResult(this.previousOperation.action.apply(null, this.operands.asArray()));
        this._setBufferOperation(this.previousOperation);
        this._addOperationToHistory(this.previousOperation.symbol, this.operands.asArray(), this.result);
        this._setBufferOperand(this.operands.get());
        this.operands.clear();
        this.operands.add(this.result);
    }
    _addOperand(operand) {
        if (this._isOperand(this._getBufferCommand())) {
            this.operands.clear();
        }
        else if (this.previousOperation.type === OPERATION_TYPES.CALCULATION) {
            this.operands.clear();
            this._clearPreviousOperation();
            this._clearResult();
        }
        if (this.previousOperation.type === undefined && this.operands.getCount() === 2) {
            this.operands.clear();
        }
        this.operands.add(operand);
        this._setBufferCommand(operand);
    }
    _executeOperation(command) {
        if (!this.calculatorOperations.hasOperation(command)) return;

        this._setCurrentOperation(command);
        this._setBufferCommand(command);
        const strategy = this._getOperationStrategy();
        strategy();
    }
    _getOperationStrategy() {
        let strategy;
        switch (this.currentOperation.type) {
            case 1: {
                strategy = this._unaryOperation.bind(this);
                break;
            };
            case 2: {
                strategy = this._binaryOperation.bind(this);
                break;
            };
            case 3: {
                strategy = this._calcOperation.bind(this);
                break;
            }
            default: strategy = DEFAULT_STRATEGY;
        }
        return strategy;
    }
    _unaryOperation() {
        this._setBufferOperation(this.currentOperation);
        let operand = this.operands.get();
        const result = this.currentOperation.action(operand);
        this._addOperationToHistory(this.currentOperation.symbol, operand, result);
        this._setBufferOperand(result);
        this._setResult(result);
        this.operands.add(result);
    }
    _binaryOperation() {
        if (this.operands.getCount() === 2) {
            this._calculate();
        }
        this._setPreviousOperation(this.currentOperation);
    }
    _calcOperation() {
        if (this.previousOperation.type === OPERATION_TYPES.CALCULATION) {
            this._setPreviousOperation(this._getBufferOperation());
            this.operands.add(this._getBufferOperand());
            this._calculate();
            this._setPreviousOperation(this.currentOperation);
        }
        else {
            if (this.operands.getCount() === 2) {
                this._calculate();
                this._setPreviousOperation(this.currentOperation);
            }
            else if (this.previousOperation.type === OPERATION_TYPES.UNARY) {
                this._calculate();
            }
            if (this.previousOperation.type !== OPERATION_TYPES.BINARY) {
                this._setPreviousOperation(this.currentOperation);
            }
        }
    }
    execute(command) {
        if (this._isOperand(command)) {
            this._addOperand(command)
        } else {
            this._executeOperation(command)
        }
        return this;
    }
    getResult() {
        if (this.result !== undefined) {
            return this.result;
        }
        return this.operands.asArray()[this.operands.getCount() - 1];
    }
}

export default Calculator;