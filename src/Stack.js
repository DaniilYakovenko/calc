class Stack {
    constructor(length) {
        this.length = length || Infinity;
        this.stack = [];
    }
    add(value) {
        if (this.stack.length >= this.length) {
            this.stack.shift();
        }
        this.stack.push(value);
    }
    clear() {
        this.stack = [];
    }
    get() {
        return this.stack.pop();
    }
    asArray() {
        return this.stack;
    }
    getCount() {
        return this.stack.length;
    }
}

export default Stack;