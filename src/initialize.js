import { getElementByData, appendChildren, appendChildrenAsElements } from './utils/utils';
import { OPERATIONS } from './calculatorOperations';

const actionsMap = new Map();
const keyCodeMap = new Map();
const screenViewer = document.querySelector('.calculator__screen');
const aside = document.querySelector('.engineer-mode');
const historyList = document.querySelector('.history-list');
function initActionsMap(controller) {
    actionsMap.set('addSymbol', controller.addSymbol.bind(controller));
    actionsMap.set('removeSymbol', controller.removeSymbol.bind(controller));
    actionsMap.set('invert', controller.invert.bind(controller));
    actionsMap.set('clearAll', controller.clearAll.bind(controller));
    actionsMap.set('executeOperation', controller.executeOperation.bind(controller));
    actionsMap.set('changeMode', changeModeAction);
    actionsMap.set('showHistory', showHistoryAction);
    actionsMap.set('insertSymbols', controller.setSymbols.bind(controller));
    function showHistoryAction() {
        historyList.hidden = !historyList.hidden;
        const btn = document.querySelector('.btn-operations-history');
        if (historyList.hidden) {
            btn.textContent = 'Show History';
        }
        else {
            btn.textContent = 'Hide History';
        }
    }
    function changeModeAction() {
        aside.hidden = !aside.hidden;
        const btn = document.querySelector('.btn-trigger');
        if (aside.hidden) {
            btn.textContent = 'Engineer Mode';
        }
        else {
            btn.textContent = 'General Mode';
        }
    }
};
function initKeyCodeMap() {
    keyCodeMap.set(8, { action: 'removeSymbol' });
    keyCodeMap.set(48, { action: 'addSymbol', value: '0' });
    keyCodeMap.set(96, { action: 'addSymbol', value: '0' });
    keyCodeMap.set(49, { action: 'addSymbol', value: '1' });
    keyCodeMap.set(97, { action: 'addSymbol', value: '1' });
    keyCodeMap.set(50, { action: 'addSymbol', value: '2' });
    keyCodeMap.set(98, { action: 'addSymbol', value: '2' });
    keyCodeMap.set(51, { action: 'addSymbol', value: '3' });
    keyCodeMap.set(99, { action: 'addSymbol', value: '3' });
    keyCodeMap.set(52, { action: 'addSymbol', value: '4' });
    keyCodeMap.set(100, { action: 'addSymbol', value: '4' });
    keyCodeMap.set(53, { action: 'addSymbol', value: '5' });
    keyCodeMap.set(101, { action: 'addSymbol', value: '5' });
    keyCodeMap.set(54, { action: 'addSymbol', value: '6' });
    keyCodeMap.set(102, { action: 'addSymbol', value: '6' });
    keyCodeMap.set(55, { action: 'addSymbol', value: '7' });
    keyCodeMap.set(103, { action: 'addSymbol', value: '7' });
    keyCodeMap.set(56, { action: 'addSymbol', value: '8' });
    keyCodeMap.set(104, { action: 'addSymbol', value: '8' });
    keyCodeMap.set(57, { action: 'addSymbol', value: '9' });
    keyCodeMap.set(105, { action: 'addSymbol', value: '9' });
    keyCodeMap.set(110, { action: 'addSymbol', value: '.' });
    keyCodeMap.set(188, { action: 'addSymbol', value: '.' });
    keyCodeMap.set(191, { action: 'addSymbol', value: '.' });
    keyCodeMap.set(189, { action: 'executeOperation', value: 'deduct' });
    keyCodeMap.set(109, { action: 'executeOperation', value: 'deduct' });
    keyCodeMap.set(106, { action: 'executeOperation', value: 'multiply' });
    keyCodeMap.set(56, { action: 'executeOperation', value: 'multiply' });
    keyCodeMap.set(111, { action: 'executeOperation', value: 'divide' });
    keyCodeMap.set(191, { action: 'executeOperation', value: 'divide' });
    keyCodeMap.set(220, { action: 'executeOperation', value: 'divide' });
    keyCodeMap.set(226, { action: 'executeOperation', value: 'divide' });
    keyCodeMap.set(107, { action: 'executeOperation', value: 'sum' });
    keyCodeMap.set(187, { action: 'executeOperation', value: 'sum' });
    keyCodeMap.set(13, { action: "executeOperation", value: 'calc' });
};
function initEvents(ea) {
    const generalButtons = Array.from(document.querySelector('.general-mode').querySelectorAll('.btn'));
    const mathOperationsButtons = Array.from(document.querySelectorAll('.btn-orange'));

    function doAction(action, value) {
        if (actionsMap.has(action)) {
            actionsMap.get(action)(value);
        }
    }
    function onElementClick(event) {
        const value = event.target.dataset.value;
        const action = event.target.dataset.action;
        doAction(action, value);
    }
    function dispatchClickEvent(obj) {
        const elem = getElementByData(obj);
        if (elem) {
            elem.dispatchEvent(new Event('click', { bubbles: true, cancelable: true }));
        }
    }
    function onValueChanged(value) {
        setScreenValue(value);
    }
    function setScreenValue(value) {
        if (screenViewer)
            screenViewer.textContent = value;
    }
    function onOperationChanged(operation) {
        mathOperationsButtons.forEach(btn => btn.classList.remove('permanentHover'));
        const elem = getElementByData({ action: 'executeOperation', value: operation });
        if (elem && operation !== OPERATIONS.CALC) {
            elem.classList.add('permanentHover');
        }
    }
    function onHistoryChanged(history) {
        if (history) {
            appendChildrenAsElements(historyList, history, 'p');
        }
    }
    (function subscribeOnEvents() {
        ea.on('value-changed', onValueChanged);
        ea.on('operation-changed', onOperationChanged);
        ea.on('history-changed', onHistoryChanged);
    })();
    (function initBodyEvents() {
        document.body.addEventListener('keydown', e => {
            if (keyCodeMap.has(e.keyCode)) {
                const elem = getElementByData(keyCodeMap.get(e.keyCode));
                if (elem) {
                    elem.classList.add('hover');
                }
            }
        });
        document.body.addEventListener('keyup', e => {
            if (keyCodeMap.has(e.keyCode)) {
                const elem = getElementByData(keyCodeMap.get(e.keyCode));
                if (elem) {
                    elem.classList.remove('hover');
                }
            }
        });
        document.body.addEventListener('keydown', e => {
            if (keyCodeMap.has(e.keyCode)) {
                dispatchClickEvent(keyCodeMap.get(e.keyCode));
            }
            else if (e.keyCode === 86 && e.ctrlKey) {
                navigator.clipboard.readText().then(text => actionsMap.get('insertSymbols')(text));
            }
        });
        document.body.addEventListener('click', onElementClick);
    })();
    (function initButtonsEvents() {
        generalButtons.forEach(btn => {
            if (!(btn.classList.contains('btn-orange'))) {
                btn.addEventListener('click', onElementClick);
            }
            btn.addEventListener('click', e => e.stopPropagation())
        });
        document.querySelector('.btn-trigger').addEventListener('keydown', e => { e.preventDefault() });
    })();
};
function initialize(ea, controller) {
    initEvents(ea);
    initActionsMap(controller);
    initKeyCodeMap();
}
export { initialize, aside, historyList };