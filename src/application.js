import { generateEngineerCalculatorStructure } from './createStructure';
import { initialize, aside, historyList } from './initialize';
import EventAggregator from './eventAggregator';
import Controller from './Controller';
import { hideElements } from './utils/utils';
import { OPERATIONS } from './calculatorOperations';

export function implementCalculator() {
    const ea = new EventAggregator();
    const controller = new Controller(ea);
    generateEngineerCalculatorStructure(aside);
    hideElements(aside, historyList);
    initialize(ea, controller);
    (function initGlobalActions() {
        window.globalActions = {
            [OPERATIONS.CALC]: controller.executeOperation.bind(controller, OPERATIONS.CALC),
            [OPERATIONS.SUM]: controller.executeOperation.bind(controller, OPERATIONS.SUM),
            [OPERATIONS.DEDUCT]: controller.executeOperation.bind(controller, OPERATIONS.DEDUCT),
            [OPERATIONS.DIVIDE]: controller.executeOperation.bind(controller, OPERATIONS.DIVIDE),
            [OPERATIONS.MULTIPLY]: controller.executeOperation.bind(controller, OPERATIONS.MULTIPLY),
        }
    })();
};

export class App {
    constructor() {
        implementCalculator();
    }
}