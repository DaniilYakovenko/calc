import { createTable, createDecoratedButton, appendChildren } from './utils/utils';

function generateEngineerCalculatorStructure(container) {
    const table = createTable(null, [
        [createDecoratedButton('m+', 'memory-plus', 'executeOperation', ['btn', 'btn-dark-gray']), createDecoratedButton('m-', 'memory-minus', 'executeOperation', ['btn', 'btn-dark-gray'])],
        [createDecoratedButton('mc', 'memory-clear', 'executeOperation', ['btn', 'btn-dark-gray']), createDecoratedButton('mr', 'memory-read', 'executeOperation', ['btn', 'btn-dark-gray'])],
        [createDecoratedButton('(', 'openBracket', 'executeOperation', ['btn', 'btn-dark-gray']), createDecoratedButton(')', 'closeBracket', 'executeOperation', ['btn', 'btn-dark-gray'])],
        [createDecoratedButton('x^2', 'power', 'executeOperation', ['btn', 'btn-dark-gray']), createDecoratedButton('√', 'radical', 'executeOperation', ['btn', 'btn-dark-gray'])],
        [createDecoratedButton('e', 'exp', 'executeOperation', ['btn', 'btn-dark-gray']), createDecoratedButton('π', 'pi', 'executeOperation', ['btn', 'btn-dark-gray'])],
    ]);
    appendChildren(container, table);
    return container;
};

export { generateEngineerCalculatorStructure };