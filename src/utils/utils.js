function isNode(node) {
    return node instanceof Node;
};
function asNode(data) {
    return isNode(data) ? data : document.createTextNode(data);
};
function isElement(target) {
    return target instanceof Element;
};
function createElement(tagName, children) {
    const element = document.createElement(tagName);
    if (!!children) {
        appendChildren(element, children)
    }
    return element;
};
function createTable(header, body, footer) {
    const thead = createElement('thead');
    const tbody = createElement('tbody');
    const tfooter = createElement('tfooter');
    if (Array.isArray(header)) {
        appendChildren(thead, header.map(tr => createElement('tr', tr.map(th => createElement('th', th)))));
    };
    if (Array.isArray(body)) {
        appendChildren(tbody, body.map(tr => createElement('tr', tr.map(td => createElement('td', td)))));
    };
    if (Array.isArray(footer)) {
        appendChildren(tfooter, footer.map(tr => createElement('tr', tr.map(td => createElement('td', td)))));
    };
    return createElement('table', [thead, tbody, tfooter]);
};
function appendChildren(target, children) {
    if (Array.isArray(children)) {
        for (let i = 0; i < children.length; i++) {
            children[i] && target.appendChild(asNode(children[i]));
        }
    } else {
        children && target.appendChild(asNode(children));
    }

    return target;
};
function appendChildrenAsElements(target, children, tagName) {
    children && target.appendChild(createElement(tagName, children));
    return target;
};
function getElementByData(obj) {
    if (obj.value) {
        return document.querySelector(`[data-value=\"${obj.value}\"][data-action=\"${obj.action}\"]`);
    }
    return document.querySelector(`[data-action='${obj.action}']`);
};
const createButton = createElement.bind(null, 'button');
function createDecoratedButton(children, dataValue, dataAction, classList) {
    const btn = createButton(children);
    btn.setAttribute('data-value', dataValue);
    btn.setAttribute('data-action', dataAction);
    if (Array.isArray(classList)) {
        btn.classList.add(...classList);
    }
    return btn;
};
function hideElements() {
    for (let i = 0, max = arguments.length; i < max; i++) {
        if (isElement(arguments[i])) {
            arguments[i].hidden = true;
        }
    }
    return arguments.length > 1 ? Array.from(arguments) : arguments[0];
};

export { createTable, createDecoratedButton, getElementByData, appendChildren, hideElements, appendChildrenAsElements };